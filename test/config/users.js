export default {
    account : {
        account : "admin",
        pwd : "123456",
        email : "account@gmail.com",
        mobile_type : "Htc",
        group : 0,
        os : "android",
        dev_token : "redsfhasjl",
        lang : "tw"
    },

    fb_user : {
        account : "facebook",
        email : "fb@gmail.com",
        fb_id : "123456ueiuwjk",
        dev_id : "fdjaslkjflsaj12sadd",
        mobile_type : "IPhone",
        os : "ios",
        dev_token : "redsfhasjdsfwel",
        lang : "tw"
    },

    twitter_user : {
        account : "twitter",
        email : "twitter@gmail.com",
        twitter_id : "123456ueiuwjkdfasfa",
        dev_id : "fdjaslkjflsajddaadc",
        mobile_type : "Htc",
        os : "android",
        dev_token : "redsfhasjlwew",
        lang : "tw"
    },

    wechat_user : {
        account : "wechat",
        email : "wechat@gmail.com",
        wechat_id : "123456ueiuwjkdfasfa",
        dev_id : "fdjaslkjfeelsaj",
        mobile_type : "Htc",
        os : "android",
        dev_token : "rdeddddsfhasjl",
        lang : "cn"
    },

    list : {

        skip : 0,
        limit : 10,
        sort : "-created_time",
        select : "account fb_id twitter_id webchat_id"

    }
};
