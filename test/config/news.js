
export default {

    toall : {
        title : "to all",
        content : "所有人都抓得到",
        to : "all",
        lang : "en",
        customer : "all",
        os : "ios"
    },

    member : {
        title : "to all",
        content : "所有人都抓得到",
        to : "member",
        lang : "tw",
        customer : "member",
        os : "all"
    },

    notmember : {
        title : "to all",
        content : "所有人都抓得到",
        to : "notmember",
        lang : "cn",
        customer : "notmember",
        os : "android"
    },

    list : {
        limit : 10,
        skip : 0,
        select : "created_time title to os",
        sort:"-created_time",
    }
};
