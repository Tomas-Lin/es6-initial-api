import should from "should";
import { makerequest } from '../../lib/Global';
import {account, fb_user, twitter_user, wechat_user} from "../../config/users";

export default {

    dev_idNotFound : (done) => {

        makerequest(
            "/users/created/v1/",
            account)
        .then(function(res){

            res.body.should.have.property("status", 424);
            res.body.should.have.property("message");
            res.body.should.not.have.property("data");
            done();

        }, function(err){

            done(err);

        });

    },

    accout_user : (done) => {

        account.dev_id = "12313";
        makerequest(
            "/users/created/v1/",
            account)
        .then(function(res){

            res.body.should.have.property("status", 200);
            res.body.should.have.property("message");
            res.body.should.have.property("data");
            res.body.data.should.have.property("token");
            res.body.data.should.have.property("os");
            res.body.data.should.have.property("dev");
            res.body.data.should.have.property("mobile_type");
            done();

        }, function(err){

            done(err);

        });

    },

    FB_user : (done) => {

        makerequest(
            "/users/created/v1/",
            fb_user)
        .then(function(res){

            res.body.should.have.property("status", 200);
            res.body.should.have.property("message");
            res.body.should.have.property("data");
            res.body.data.should.have.property("token");
            res.body.data.should.have.property("os");
            res.body.data.should.have.property("dev");
            res.body.data.should.have.property("mobile_type");
            done();

        }, function(err){

            done(err);

        });

    },

    Twitter_user : (done) => {

        makerequest(
            "/users/created/v1/",
            twitter_user)
        .then(function(res){

            res.body.should.have.property("status", 200);
            res.body.should.have.property("message");
            res.body.should.have.property("data");
            res.body.data.should.have.property("token");
            res.body.data.should.have.property("os");
            res.body.data.should.have.property("dev");
            res.body.data.should.have.property("mobile_type");
            done();

        }, function(err){

            done(err);

        });

    },

    Wechat_user : (done) => {

        makerequest(
            "/users/created/v1/",
            wechat_user)
        .then(function(res){

            res.body.should.have.property("status", 200);
            res.body.should.have.property("message");
            res.body.should.have.property("data");
            res.body.data.should.have.property("token");
            res.body.data.should.have.property("os");
            res.body.data.should.have.property("dev");
            res.body.data.should.have.property("mobile_type");
            done();

        }, function(err){

            done(err);

        });

    }

}
