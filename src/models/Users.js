import _ from "lodash";
import Promise from "bluebird";
import ErrorManager from "../lib/ErrorManager";
import Collection from "../lib/MongoBase";
import { Schema } from "mongoose";
import { now } from "../lib/TimeBase";

class UsersClass extends Collection{

    constructor(name, schema){

        schema.methods.usefulprods = (prods) => {

            return _.filter(prods, (prod) => {

                return prod.fail === 0;

            });
        }
        super(name, schema);

    }

    getprodfield(prod){
        return _.pick(prod
                , "_id", "code", "name"
                , "created_time", "csv_url"
                , "version", "zip_url", "mainpic");
    }

    usefulprods(prods){

        return _.map(prods, (prod) => {
            return prod.fail === 0;

        });
    }

    push_prod(user, start_code, prod){

        return new Promise((resolve, reject) => {

            let updatequery = this.getprodfield(prod);
            updatequery.start_code = start_code;
            updatequery.deal_time = now();

            user.prods.push(updatequery);
            user.save((err, user) => {

                if(err){

                    reject(ErrorManager.GetDBUpdateError("產品加入失敗"));

                }else{

                    resolve(user);

                }
            });
        }.bind(this));
    }
}

const ProdSchema = new Schema({

    /**
     * fail : 啟動碼作廢
     *
    * **/
    pid : {type : Schema.Types.ObjectId},

    code : {type : String},

    name : {type : String},

    start_code : {type : String},

    created_time : {type : Number},

    version : {type : Number},

    csv_url : {type : String},

    zip_url : {type : String},

    deaded_time : {type : Number},

    deal_time : {type : Number},

    type : {type : Number},

    mainpic : {type : String},

    status : {
        type : Number,
        default : 1
    },

    fail : {
        type : Number,
        default : 0
    }

});

let UserSchema = new Schema({

    account : {
        type : String,
        unique:true
    },

    prods : [ProdSchema],

    group : {
        type : Number,
        enum : [0, 1, 2, 3, 4],
        default : 3
    },

    pwd : {
        type : String
    },

    fb_id : {
        type : String
    },

    created_time : {
        type : Number,
        required : true
    },

    twitter_id : {
        type : String
    },

    wechat_id : {
        type : String
    },

    status : {
        type : Number,
        enum : [0, 1, 2]
    },

    lock_time : {
        type : Number,
        default : now()
    },

    dev : {

        dev_id : {
            type : String,
            required : true
        },

        os : {
            type : String
        },

        token : {
            type : String
        },

        mobile_type : {
            type : String
        },

        login : {

            type : Number,
            required : true

        }

    },

    change_count : {
        type : Number,
        default : 0
    },

    old_dev : {

        dev_id : {
            type : String,
            default : "未使用"
        },

        os : {
            type : String,
            default : "未使用"
        },

        token : {
            type : String,
            default : "未使用"
        },

        mobile_type : {
            type : String,
            default : "未使用"
        },

        login : {
            type : Number
        }

    },

    try : {
        type : Number,
        default : 0
    },

    lang : {
        type : String
    }

});

let Users = new UsersClass("user", UserSchema);

export default Users;

