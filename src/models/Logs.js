import Collection from "../lib/MongoBase";
import { Schema } from "mongoose";

class LogsClass extends Collection{

    constructor(name, schema){

        super(name, schema);

    }

}

let Logs = new LogsClass("log", new Schema({

    coll : {
        type : String
    },

    method : {
        type : String
    },

    created_time : {
        type : Number
    },

    query : {},

    doc : {}

}));

export default Logs;
