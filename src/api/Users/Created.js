import { Users } from '../../models';
import { CheckEmpty} from '../../lib/util';
import { getFreshToken } from '../../lib/LoginBase';
import ErrorManager from '../../lib/ErrorManager';
import Time from '../../lib/TimeBase';
import _ from 'lodash';

export default {
    post : (req, res, next) => {

        let query = _.pick(req.body
                , "account", "pwd", "fb_id"
                , "twitter_id", "email", "lang"
                , "group");

        if(CheckEmpty(req.body.dev_id)){

            req.error = ErrorManager.GetEmptyReqError("裝置ID不可為空");
            next();

        }else{

            query.dev = _.pick(req.body
                    , "dev_id", "os", "mobile_type");

            query.dev.token = req.body.dev_token;

            query.dev.login = Time.now();

            query.created_time = Time.now();

            Users
                .created(query)
                .then(function(user){

                    const token = getFreshToken(user);

                    req.result = {

                        token : token,
                        os : user.dev.os,
                        dev : user.dev.dev_id,
                        mobile_type : user.dev.mobile_type

                    };

                    req.message = "會員新增成功";
                    next();

                }, function(err){

                    req.error = ErrorManager.GetDBInsertError("新增會員失敗");
                    next();

                });
        }
    }
};

